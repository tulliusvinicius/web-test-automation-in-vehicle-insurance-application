Feature: Cotação de seguro


  Scenario Outline: Deve preencher o formulário “Enter Vehicle Data”
    Given que desejo preencher o formulário vehicle data
    When preencho os campos: <make>, <enginePerformance>, <dataManufature>, <numberSeats>, <fuelType>, <listPrice>, <licencePlateNumber>, <annualMileage>
    Then verifico que ficou sem pendência em vehicle data

    Examples:
      | make   | enginePerformance | dataManufature | numberSeats | fuelType | listPrice | licencePlateNumber | annualMileage |
      | "Audi" | "777"             | "07/07/2017"   | "3"         | "Diesel" | "10000"   | "700"              | "300"         |


  Scenario Outline: Deve preencher o formulário “Enter Insurant Data”
    Given que desejo preencher o formulário insurant data
    When preencho os campos: <firstName>, <lastName>, <dateBirth>, <gender>, <streetAddress>, <country>, <zipcode> <city>, <occupation>, <hobbies>, <website>
    Then verifico que ficou sem pendência em insurant data

    Examples:
      | firstName | lastName | dateBirth    | gender | streetAddress | country  | zipcode | city     | occupation | hobbies        | website            |
      | "Teste"   | "Last"   | "09/27/1991" | "Male" | "Rua teste"   | "Brazil" | "5050"  | "Recife" | "Employee" | "Cliff Diving" | "http://teste.com" |


  Scenario Outline: Deve preencher o formulário “Enter Product Data”
    Given que desejo preencher o formulário product data
    When preencho os campos: <startDate>, <insuranceSum>, <meritRating>, <damageInsurance>, <optionalProducts>, <courtesyCar>
    Then verifico que ficou sem pendência em product data

    Examples:
      | startDate    | insuranceSum   | meritRating | damageInsurance | optionalProducts  | courtesyCar |
      | "01/01/2024" | "7.000.000,00" | "Bonus 5"   | "Full Coverage" | "Euro Protection" | "No"        |


  Scenario: Deve selecionar um plano “Select Price Option”
    Given que desejo selecionar uma opção de preço
    When escolho: "Platinum"
    Then verifico que ficou sem pendência em select price option


  Scenario Outline: Deve preencher o formulário “Send Quote”
    Given que desejo preencher o formulário send quote
    When preencho os campos: <email>, <userName>, <password>, <confirmPassword>
    Then verifico que a cotação foi enviada com sucesso


    Examples:
      | email             | userName         | password  | confirmPassword |
      | "email@teste.com" | "UserName" | "Pass123" | "Pass123"       |
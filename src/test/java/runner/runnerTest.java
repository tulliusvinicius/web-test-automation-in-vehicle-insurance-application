package runner;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features/desafio.feature",
        glue = "steps",
        plugin = {"pretty", "html:target/Reports/report.html"},
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        dryRun = false
)

public class runnerTest {
}


// teste git
package steps;

import core.Utils;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.After;
import io.cucumber.java.AfterAll;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.*;

public class AutomobileSteps {

    private final HomePage home = new HomePage();
    private final VehicleDataPage vehicleData = new VehicleDataPage();
    private final InsurantDataPage insurantData = new InsurantDataPage();
    private final ProductDataPage productData = new ProductDataPage();
    private final PriceOptionPage priceOption = new PriceOptionPage();
    private final SendQuotePage sendQuote = new SendQuotePage();

    @Given("que desejo preencher o formulário vehicle data")
    public void queEuAcessoOSite() {
        home.acessarTelaInicial("http://sampleapp.tricentis.com/101/index.php");
        Utils.captureScreenshot();
        home.ClicarBotaoAutomobile();
        Utils.captureScreenshot();
    }

    @When("preencho os campos: {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}")
    public void preenchoOsCampos(String make, String enginePerformance, String dataManufature, String numberSeats, String fuelType, String listPrice, String licencePlateNumber, String annualMileage) {
        vehicleData.selectComboMake(make);
        vehicleData.fillEnginePerformance(enginePerformance);
        vehicleData.fillDateOfManufacture(dataManufature);
        vehicleData.selectNumberOfSeats(numberSeats);
        vehicleData.selectFuelType(fuelType);
        vehicleData.fillListPrice(listPrice);
        vehicleData.fillLicencePlateNumber(licencePlateNumber);
        vehicleData.fillAnnualMileage(annualMileage);
    }

    @Then("verifico que ficou sem pendência em vehicle data")
    public void verificoQueFicouSemPendênciaVehicleData() {
        Utils.captureScreenshot();
        vehicleData.checkPendencyVehicleData();
    }

    // Insurant Data
    @Given("que desejo preencher o formulário insurant data")
    public void queDesejoPreencherOFormulárioInsurantData() {
        vehicleData.clickButtonNextToInsurantData();
    }

    @When("preencho os campos: {string}, {string}, {string}, {string}, {string}, {string}, {string} {string}, {string}, {string}, {string}")
    public void preenchoOsCampos(String firstName, String lastName, String dateBirth, String gender, String streetAddress, String country, String zipcode, String city, String occupation, String hobbies, String website) {
        Utils.captureScreenshot();
        insurantData.fillFirstName(firstName);
        insurantData.fillLastName(lastName);
        insurantData.fillDateBirth(dateBirth);
        insurantData.selectGender(gender);
        insurantData.fillStreetAddress(streetAddress);
        insurantData.selectCountry(country);
        insurantData.fillZipCode(zipcode);
        insurantData.fillCity(city);
        Utils.captureScreenshot();
        insurantData.selectOccupation(occupation);
        insurantData.selectHobbies(hobbies);
        insurantData.fillWebSite(website);
        insurantData.checkPendencyInsurantData();
    }

    @Then("verifico que ficou sem pendência em insurant data")
    public void verificoQueFicouSemPendênciaInsurantData() {
        Utils.captureScreenshot();
        insurantData.checkPendencyInsurantData();
    }

    // Product Data
    @Given("que desejo preencher o formulário product data")
    public void que_desejo_preencher_o_formulário_product_data() {
        insurantData.clickButtonNextToProductData();
    }

    @When("preencho os campos: {string}, {string}, {string}, {string}, {string}, {string}")
    public void preenchoOsCampos(String startDate, String insuranceSum, String meritRating, String damageInsurance, String optionalProducts, String courtesyCar) {
        Utils.captureScreenshot();
        productData.fillStartDate(startDate);
        productData.selectInsuranceSum(insuranceSum);
        productData.selectMeritRating(meritRating);
        productData.selectDamageInsurance(damageInsurance);
        productData.selectOptionalProducts(optionalProducts);
        productData.selectCourtesyCar(courtesyCar);
        productData.checkPendencyProductData();
    }

    @Then("verifico que ficou sem pendência em product data")
    public void verificoQueFicouSemPendênciaProductData() {
        Utils.captureScreenshot();
        productData.checkPendencyProductData();
    }

    // Select Price Option
    @Given("que desejo selecionar uma opção de preço")
    public void queDesejoSelecionarUmaOpçãoDePreço() {
        productData.clickButtonNextToPriceOption();
    }

    @When("escolho: {string}")
    public void escolho(String option) {
        Utils.captureScreenshot();
        priceOption.selectPriceOption(option);
    }

    @Then("verifico que ficou sem pendência em select price option")
    public void verificoQueFicouSemPendênciaEmSelectPriceOption() {
        Utils.captureScreenshot();
        priceOption.checkPendencyPriceOption();
    }


    // Send Quote
    @Given("que desejo preencher o formulário send quote")
    public void queDesejoPreencherFormulárioSendQuote() {
        priceOption.clickButtonNextToSendQuote();
    }

    @When("preencho os campos: {string}, {string}, {string}, {string}")
    public void preenchoOsCampos(String email, String userName, String password, String confirmPassword) {
        Utils.captureScreenshot();
        sendQuote.fillEmail(email);
        sendQuote.fillUsername(userName);
        sendQuote.fillPassword(password);
        sendQuote.fillConfirmPassword(confirmPassword);
        Utils.captureScreenshot();
    }

    @Then("verifico que a cotação foi enviada com sucesso")
    public void verifico_que_a_cotação_foi_enviada_com_sucesso() {
        sendQuote.clickButtonSend();
        Utils.captureScreenshot();
        sendQuote.validSuccessMessage();
        Utils.captureScreenshot();
        sendQuote.clickButtonConfirm();
        Utils.captureScreenshot();
    }


    //@Then("Verificar mensagem de enviado com Sucesso na aba “Send Quote”.")
    public void verificarMensagemDeEnviadoComSucessoNaAbaSendQuote() {
        Utils.captureScreenshot();
        sendQuote.validSuccessMessage();
        Utils.captureScreenshot();
        sendQuote.clickButtonConfirm();
    }


    //@AfterAll
    public void closeBrowser() {
        home.closedBrowser();
    }

}

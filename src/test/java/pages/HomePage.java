package pages;

import org.openqa.selenium.By;

import static core.DriverFactory.getDriver;

public class HomePage extends BasePage {
    private By buttonAutomobile = By.xpath("//div[@class='main-navigation']//a[@id='nav_automobile']");

    public void acessarTelaInicial(String site) {
        getDriver().get(site);
    }

    public void ClicarBotaoAutomobile() {
        click(buttonAutomobile);
    }
}

package pages;

import org.openqa.selenium.By;

public class InsurantDataPage extends BasePage {

    // Variáveis contendo todos os locators da aba Insurant Data para preenchimento

    private By fieldFirstName = By.id("firstname");
    private By fieldLastName = By.id("lastname");
    private By fieldDateBirth = By.id("birthdate");
    private String radioGenderMale = "gendermale";
    private String radioGenderFemale = "genderfemale";
    private By fieldStreetAddress = By.id("streetaddress");
    private By comboCountry = By.id("country");
    private By fieldZipCode = By.id("zipcode");
    private By fieldCity = By.id("city");
    private By comboOccupation = By.id("occupation");
    private String ckeckHobbieOptionsSpeeding = "speeding";
    private String ckeckHobbieOptionsBungeeJumping = "bungeejumping";
    private String ckeckHobbieOptionsCliffDiving = "cliffdiving";
    private String ckeckHobbieOptionsSkyDiving = "skydiving";
    private String ckeckHobbieOptionsOther = "other";
    private By fieldWebSite = By.id("website");
    private String pendencyValueInsurantData = "Enter Insurant Data";
    private String buttonNextToProductData = "nextenterproductdata";


    // Métodos utilizados na classe step AutomobileSteps

    public void fillFirstName(String firstName) {
        write(fieldFirstName, firstName);
    }

    public void fillLastName(String lastName) {
        write(fieldLastName, lastName);
    }

    public void fillDateBirth(String dateBirth) {
        write(fieldDateBirth, dateBirth);
    }

    public void selectGender(String gender) {
        if (gender.equals("Male")) {
            clickRadio(radioGenderMale);
        } else if (gender.equals("Female")) {
            clickRadio(radioGenderFemale);
        }
    }

    public void fillStreetAddress(String streetAddress) {
        write(fieldStreetAddress, streetAddress);
    }

    public void selectCountry(String country) {
        selectCombobox(comboCountry, country);
    }

    public void fillCity(String city) {
        write(fieldCity, city);
    }

    public void fillZipCode(String zipCode) {
        write(fieldZipCode, zipCode);
    }

    public void selectOccupation(String occupation) {
        selectCombobox(comboOccupation, occupation);
    }

    public void selectHobbies(String hobbies) {
        if (hobbies.equals("Speeding")) {
            clickCheckBox(ckeckHobbieOptionsSpeeding);
        } else if (hobbies.equals("Bungee Jumping")) {
            clickCheckBox(ckeckHobbieOptionsBungeeJumping);
        } else if (hobbies.equals("Cliff Diving")) {
            clickCheckBox(ckeckHobbieOptionsCliffDiving);
        } else if (hobbies.equals("Skydiving")) {
            clickCheckBox(ckeckHobbieOptionsSkyDiving);
        } else if (hobbies.equals("Other")) {
            clickCheckBox(ckeckHobbieOptionsOther);
        }
    }

    public void fillWebSite(String website) {
        write(fieldWebSite, website);
    }

    public void checkPendencyInsurantData() {
        checkPendency(pendencyValueInsurantData);
    }

    public void clickButtonNextToProductData() {
        clickNext(buttonNextToProductData);
    }

}

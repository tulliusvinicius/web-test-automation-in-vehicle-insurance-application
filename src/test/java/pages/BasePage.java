package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import static core.DriverFactory.getDriver;
import static core.DriverFactory.killDriver;

public class BasePage {

    // método genérico para clicar recebendo o locator
    public void click(By by) {
        getDriver().findElement(by).click();
    }

    // método genérico para escrever recebendo o locator e enviando uma string
    public void write(By by, String locator) {
        getDriver().findElement(by).clear();
        getDriver().findElement(by).sendKeys(locator);
    }

    // método genérico para selecionar um box recebendo o locator e selecionando o valor passado na String
    public void selectCombobox(By id, String valor) {
        WebElement element = getDriver().findElement(id);
        Select combo = new Select(element);
        combo.selectByVisibleText(valor);
    }

    // método genérico para clicar em um radio button recebendo o locator
    public void clickRadio(String idRadio) {
        click(By.xpath(String.format("//input[@id='%s']//..//span", idRadio)));
    }

    // método genérico para validar se faltou preencher algum campo obrigatório
    // recebendo o atributo @name e substituindo em %s
    public void checkPendency(String namePageData) {
        String text = getDriver().findElement(By.xpath(String.format("//*[@name='%s']//span", namePageData))).getText();
        Assert.assertEquals("0", text);
    }

    // método genérico para a aba de formulário atual. compara se a aba esperada é igual a atual
    public void checkActualTab(String expectedTab) {
        WebElement text = getDriver().findElement(By.xpath("//*[@class='idealsteps-step-active']//a"));
        String actualTab = text.getAttribute("name");
        Assert.assertEquals(expectedTab, actualTab);
    }

    // método genérico para clicar no botão de next passando um locator
    public void clickNext(String IdNextButtonData) {
        click(By.id(String.format("%s", IdNextButtonData)));
    }

    // método genérico para clicar num checkbox recebendo o atributo @id e substituindo em %s
    public void clickCheckBox(String idCheckBox) {
        click(By.xpath(String.format("//*//input[@id='%s']//..//span", idCheckBox)));
    }

    // método genérico para capturar um texto passando o locator
    public String getText(By by) {
        return getDriver().findElement(by).getText();
    }

    // método genérico para fechar o browser
    public void closedBrowser() {
        killDriver();
    }
}
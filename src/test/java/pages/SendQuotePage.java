package pages;

import org.junit.Assert;
import org.openqa.selenium.By;

public class SendQuotePage extends BasePage {

    // Variáveis contendo todos os locators da aba Send Quote para preenchimento

    private By fieldEmail = By.id("email");
    private By fieldUsername = By.id("username");
    private By fieldPassword = By.id("password");
    private By fieldConfirmPassword = By.id("confirmpassword");
    private String pendencyValueInsurantData = "Enter Insurant Data";
    private String buttonSend = "sendemail";
    private By actualTextMessage = By.xpath("//h2");
    private String expectedTextSuccessMessage = "Sending e-mail success!";
    private By buttonConfirm = By.xpath("//button[@class='confirm']");


    // Métodos utilizados na classe step AutomobileSteps

    public void fillEmail(String email) {
        write(fieldEmail, email);
    }

    public void fillUsername(String username) {
        write(fieldUsername, username);
    }

    public void fillPassword(String password) {
        write(fieldPassword, password);
    }

    public void fillConfirmPassword(String confirmPassword) {
        write(fieldConfirmPassword, confirmPassword);
    }

    public void checkPendencyInsurantData() {
        checkPendency(pendencyValueInsurantData);
    }

    public void clickButtonSend() {
        clickNext(buttonSend);
    }

    public void validSuccessMessage() {
        Assert.assertEquals(expectedTextSuccessMessage, getText(actualTextMessage));
    }

    public void clickButtonConfirm() {
        click(buttonConfirm);
    }

}

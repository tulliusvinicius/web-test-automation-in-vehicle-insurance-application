package pages;

import org.openqa.selenium.By;

public class ProductDataPage extends BasePage {

    // Variáveis contendo todos os locators da aba Product Data para preenchimento

    private By fieldStartDate = By.id("startdate");
    private By comboInsuranceSum = By.id("insurancesum");
    private By comboMeritRating = By.id("meritrating");
    private By comboDamageInsurance = By.id("damageinsurance");
    private String ckeckOptionalProductsEuroProtection = "EuroProtection";
    private String ckeckOptionalProductsLegalDefenseInsurance = "LegalDefenseInsurance";
    private By comboCourtesyCar = By.id("courtesycar");
    private String pendencyValueProductData = "Enter Product Data";
    private String buttonNextToPriceOption = "nextselectpriceoption";


    // Métodos utilizados na classe step AutomobileSteps

    public void fillStartDate(String startDate) {
        write(fieldStartDate, startDate);
    }

    public void selectInsuranceSum(String insuranceSum) {
        selectCombobox(comboInsuranceSum, insuranceSum);
    }

    public void selectMeritRating(String meritRating) {
        selectCombobox(comboMeritRating, meritRating);
    }

    public void selectDamageInsurance(String damageInsurance) {
        selectCombobox(comboDamageInsurance, damageInsurance);
    }

    public void selectOptionalProducts(String optionalProduct) {
        if (optionalProduct.equals("Euro Protection")) {
            clickCheckBox(ckeckOptionalProductsEuroProtection);
        } else if (optionalProduct.equals("Legal Defense Insurance")) {
            clickCheckBox(ckeckOptionalProductsLegalDefenseInsurance);
        }
    }

    public void selectCourtesyCar(String courtesyCar) {
        selectCombobox(comboCourtesyCar, courtesyCar);
    }

    public void checkPendencyProductData() {
        checkPendency(pendencyValueProductData);
    }

    public void clickButtonNextToPriceOption() {
        clickNext(buttonNextToPriceOption);
    }

}

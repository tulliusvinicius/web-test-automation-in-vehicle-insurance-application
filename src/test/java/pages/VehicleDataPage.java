package pages;

import org.openqa.selenium.By;

public class VehicleDataPage extends BasePage {

    // Variáveis contendo todos os locators da aba Vehicle Data para preenchimento

    private final By comboEnginePerformance = By.id("make");
    private final By fieldEnginePerformance = By.id("engineperformance");
    private final By fieldDateOfManufacture = By.id("dateofmanufacture");
    private final By comboNumberOfSeats = By.id("numberofseats");
    private final By comboFuelType = By.id("fuel");
    private final By fieldListPrice = By.id("listprice");
    private final By fieldLicencePlateNumber = By.id("licenseplatenumber");
    private final By fieldAnnualMileage = By.id("annualmileage");
    private final String pendencyValueVehicleData = "Enter Vehicle Data";
    private final String buttonNextToInsurantData = "nextenterinsurantdata";


    // Métodos utilizados na classe step AutomobileSteps

    //    public void checkVehicleDataTab(String tabName){
//        checkActualTab(tabName);
//    }
    public void selectComboMake(String make) {
        selectCombobox(comboEnginePerformance, make);
    }

    public void fillEnginePerformance(String enginePerformance) {
        write(fieldEnginePerformance, enginePerformance);
    }

    public void fillDateOfManufacture(String dateOfManufacture) {
        write(fieldDateOfManufacture, dateOfManufacture);
    }

    public void selectNumberOfSeats(String numberOfSeates) {
        selectCombobox(comboNumberOfSeats, numberOfSeates);
    }

    public void selectFuelType(String fuelType) {
        selectCombobox(comboFuelType, fuelType);
    }

    public void fillListPrice(String listPrice) {
        write(fieldListPrice, listPrice);
    }

    public void fillLicencePlateNumber(String licencePlateNumber) {
        write(fieldLicencePlateNumber, licencePlateNumber);
    }

    public void fillAnnualMileage(String annualMileage) {
        write(fieldAnnualMileage, annualMileage);
    }

    public void checkPendencyVehicleData() {
        checkPendency(pendencyValueVehicleData);
    }

    public void clickButtonNextToInsurantData() {
        clickNext(buttonNextToInsurantData);
    }

}

package pages;

import core.DriverFactory;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static core.DriverFactory.getDriver;
import static java.lang.Thread.*;

public class PriceOptionPage extends BasePage {

    // Variáveis contendo todos os locators da aba Price Option para preenchimento
    private String radioSelectOptionSilver = "selectsilver";
    private String radioSelectOptionGold = "selectgold";
    private String radioSelectOptionPlatinum = "selectplatinum";
    private String radioSelectOptionUltimate = "selectultimate";
    private String pendencyValuePriceOption = "Select Price Option";
    private String buttonNextToSendQuote = "nextsendquote";


    // Métodos utilizados na classe step AutomobileSteps
    public void selectPriceOption(String priceOption) {
        if (priceOption.equals("Silver")) {
            clickRadio(radioSelectOptionSilver);
        } else if (priceOption.equals("Gold")) {
            clickRadio(radioSelectOptionGold);
        } else if (priceOption.equals("Platinum")) {
            clickRadio(radioSelectOptionPlatinum);
        } else if (priceOption.equals("Ultimate")) {
            clickRadio(radioSelectOptionUltimate);
        }
    }

    public void checkPendencyPriceOption() {
        checkPendency(pendencyValuePriceOption);
    }

    public void clickButtonNextToSendQuote() {
        clickNext(buttonNextToSendQuote);
    }

}
